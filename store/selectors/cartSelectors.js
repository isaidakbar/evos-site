export const cartTotalPriceSelector = (state) => {
  return state.cart.cartItems.reduce(
    (total, cartItem) =>
      (total += cartItem.price.salePrice * cartItem.quantity),
    0
  )
}

export const cartItemsTotalQuantitySelector = (state) => {
  return state.cart.cartItems.reduce(
    (total, cartItem) => (total += cartItem.quantity),
    0
  )
}
