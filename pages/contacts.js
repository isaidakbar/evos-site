import React from 'react'
import { Box, Container, Typography, Divider } from '@mui/material'
import CardWrapper from '../components/CardWrapper/CardWrapper'
import SEO from '../components/seo'

export default function Contacts() {
  return (
    <>
      <SEO title='Контакт' />
      <Box id='boxRoot'>
        <Container maxWidth='xl'>
          <Box id='boxFullWidthBig'>
            <Box display='flex'>
              <Box width='100%'>
                <CardWrapper>
                  <Box
                    display='flex'
                    justifyContent='space-between'
                    flexDirection='column'
                  >
                    <Typography
                      variant={`h1`}
                      color={`black`}
                      marginBottom='20px'
                    >
                      Контакты
                    </Typography>
                    <Box
                      display='flex'
                      justifyContent='space-between'
                      alignItems='center'
                      marginBottom='10px'
                    >
                      <Typography color={`black`}>Call-центр</Typography>
                      <Typography
                        variant={`h20`}
                        sx={{ display: 'flex', flexDirection: 'column' }}
                      >
                        <a className='phoneNumbers' href='tel:+998712031212'>
                          +998 71-203-12-12
                        </a>{' '}
                        <a className='phoneNumbers' href='tel:+998712035555'>
                          +998 71-203-55-55
                        </a>
                      </Typography>
                    </Box>
                    <Divider />
                    <Typography variant={`h19`} sx={{ marginTop: '30px' }}>
                      Вы можете написать нам{' '}
                      <a
                        style={{ color: '#309B42' }}
                        href='https://t.me/evosdeliverybot'
                        target='_blank'
                        rel='noreferrer'
                      >
                        https://t.me/evosdeliverybot
                      </a>
                      .
                    </Typography>
                  </Box>
                </CardWrapper>
              </Box>
            </Box>
          </Box>
        </Container>
      </Box>
    </>
  )
}
