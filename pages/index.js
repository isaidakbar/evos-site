import dynamic from 'next/dynamic'
// import Banner from '../components/Banner/Banner'
import SEO from '../components/seo'
// import axios from '../utils/axios'
import { fetchMultipleUrls } from '../utils/fetchMultipleUrls'

const HeroSection = dynamic(() =>
  import('../components/HeroSection/HeroSection')
)
const Menu = dynamic(() => import('../components/Menu/Menu'))
const MobileMenu = dynamic(() => import('../components/Menu/MobileMenu'))
const Products = dynamic(() => import('../components/Products/Products'))
const ToTop = dynamic(() => import('../components/ToTop/ToTop'))

export default function Home({ menu }) {
  // console.log('menu => ', menu)

  // function getNearestBranch(long, lat, companyId = '1') {
  //   const coords = [long, lat].join(',')
  //   axios
  //     .get(`/branch/nearby/${coords}/${companyId}`)
  //     .then((res) => console.log('res => ', res))
  //     .catch((err) => console.log(err))
  // }

  // useEffect(() => {
  //   navigator.geolocation.getCurrentPosition(
  //     function (position) {
  //       console.log('Latitude is :', position.coords.latitude)
  //       console.log('Longitude is :', position.coords.longitude)
  //       // getNearestBranch(position.coords.latitude, position.coords.longitude)
  //     },
  //     function (error) {
  //       console.error('Error Code = ' + error.code + ' - ' + error.message)
  //     }
  //   )
  // }, [])

  return (
    <div>
      <SEO title='Главная' />
      {/* <Banner /> */}
      <HeroSection />
      <Menu menu={menu} />
      <MobileMenu menu={menu} />
      <Products menu={menu} />
      <ToTop />
    </div>
  )
}

export async function getServerSideProps() {
  const [menu, categories] = await fetchMultipleUrls([
    '/menu/bracnh/24',
    '/menu/top',
  ])

  let formattedMenu = formatMenu()

  function formatMenu() {
    const list = categories.menues.map((item) => {
      const products = menu.menuWithDetails
        .filter((element) => element.topMenu.id === item.id)
        .map((element) => ({
          ...element.itemMenu,
          details: element.itemMenuDetails,
          price: element.itemMenuPrice,
        }))

      return {
        ...item,
        products,
      }
    })
    return list
      .filter((element) => element.products.length)
      .sort((a, b) => a.sortIndex - b.sortIndex)
  }

  return {
    props: {
      menu: formattedMenu,
    },
  }
}
