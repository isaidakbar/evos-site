import React from 'react'
import { Box, Container } from '@mui/material'
import NewsDesc from '../components/NewsDesc/NewsDesc'
import SEO from '../components/seo'

export default function AboutUs() {
  return (
    <>
      <SEO title='О Нас' />
      <Box id='boxRoot'>
        <Container maxWidth='xl'>
          <NewsDesc />
        </Container>
      </Box>
    </>
  )
}
