import React from 'react'
import BranchDesc from '../components/BranchDesc/BranchDesc'
import { Box, Container } from '@mui/material'
import SEO from '../components/seo'

function branchDescription() {
  return (
   <>
    <SEO title='Описание филиала' />
     <Box id='boxRoot'>
    <Container maxWidth='xl'>
      <BranchDesc />
    </Container>
  </Box>
   </>
  )
}

export default branchDescription