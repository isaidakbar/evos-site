import React from 'react'
import { Box, Container } from '@mui/material'
import VacancyItem from '../../components/VacancyItem/VacancyItem.jsx'
import SEO from '../../components/seo'
import { vacancies } from '../../mock/vacancies'

function Vacancy() {
  return (
    <>
      <SEO title='Вакансия' />
      <Box id='boxRoot'>
        <Container maxWidth='xl'>
          <VacancyItem data={vacancies} />
        </Container>
      </Box>
    </>
  )
}

export default Vacancy
