import React, { useState } from 'react'
import { Box, Typography, Grid } from '@mui/material'
import CardWrapper from '../CardWrapper/CardWrapper'
import cls from './VacancyItem.module.scss'
import { useRouter } from 'next/router'
import VacancyRequestForm from '../VacancyRequestDialog/VacancyRequestForm'
import { menu, btnText } from '../../mock/menu'

function VacancyItem({ data }) {
  const router = useRouter()
  const { locale } = router
  const [open, setOpen] = useState(false)

  const handleClose = () => {
    setOpen(false)
  }

  const handleOpen = () => {
    setOpen(true)
  }
  const handleRedirect = (id) => {
    router.push(`/vacancy/${id}`)
  }

  return (
    <>
      <Typography variant={`h1`} color={`black`} className={cls.title}>
        {menu[locale].vacancies}
      </Typography>

      <Box className={cls.wrapper}>
        <Grid container spacing={2}>
          {data.map((item, index) => (
            <Grid item xs={12} md={6} lg={4} key={index}>
              <CardWrapper style={{ height: '100%' }}>
                <Box
                  display='flex'
                  flexDirection='column'
                  justifyContent='space-between'
                  height='100%'
                >
                  <div>
                    <Box marginBottom='24px'>
                      <Typography variant={`h16`} sx={{ marginBottom: '24px' }}>
                        {item.title}
                      </Typography>
                    </Box>
                    {item.requirements[0].infos.slice(0, 4).map((el, ind) => (
                      <Box
                        display='flex'
                        flexDirection='column'
                        key={el.title + ind}
                      >
                        <Box display='flex' sx={{ marginBottom: '16px' }}>
                          <Typography variant={`h14`} className={cls.listStyle}>
                            {el.title}
                          </Typography>
                        </Box>
                      </Box>
                    ))}
                  </div>
                  <Box
                    display='flex'
                    alignItems='flex-end'
                    marginTop='8px'
                    flexGrow='1'
                  >
                    <button
                      className={cls.btn}
                      onClick={() => handleRedirect(item.id)}
                    >
                      {btnText[locale].details}
                    </button>
                    <button className={cls.btnPrimary} onClick={handleOpen}>
                      {btnText[locale].apply}
                    </button>
                  </Box>
                </Box>
              </CardWrapper>
            </Grid>
          ))}
        </Grid>
      </Box>

      <VacancyRequestForm
        onClose={handleClose}
        open={open}
      ></VacancyRequestForm>
    </>
  )
}

export default VacancyItem
