import React from 'react'
import {Box, Typography, Dialog, DialogTitle} from '@mui/material'
import Input from '../Input/Input'
import FullButton from '../FullButton/FullButton'
import { styled } from '@mui/material/styles';
import CloseIcon from '../CloseIcon/CloseIcon'
import cls from './MyInfoDialog.module.scss'

const BootstrapDialog = styled(Dialog)(({ theme }) => ({
    '& .MuiPaper-root': {
    //   padding: theme.spacing(2),
    margin: '0',
    padding: '32px',
    width: '434px'
    },
    '& .MuiDialogActions-root': {
      padding: theme.spacing(1),
    },
  }));

function MyInfoDialog({onClose, open}) {
  return (
    <BootstrapDialog onClose={onClose} open={open}>
    <CloseIcon style={{margin: '0'}} onClick={onClose}/>
  <Typography variant={`h1`}>Мои данные</Typography>
  <form>
      <Box marginBottom='24px' marginTop='32px'>
          <label htmlFor="name"><Typography variant={`h17`}>ФИО</Typography></label>
          <Input style={{marginTop: '8px'}} defaultValue='Abror'/>
      </Box>
      <Box marginBottom='32px'>
          <label htmlFor="name"><Typography variant={`h17`}>Телефон</Typography></label>
          <Input style={{marginTop: '8px'}} defaultValue='+998901234567'/>
      </Box>
      <Box display='flex'>
          <button className={cls.btn} onClick={onClose}>Отмена</button>
          <button className={cls.btnPrimary} onClick={onClose}>Сохранить изменения</button>
      </Box>
  </form>
</BootstrapDialog>
  )
}

export default MyInfoDialog