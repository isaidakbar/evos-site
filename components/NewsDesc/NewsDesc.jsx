import React from 'react'
import { Box, Grid, Typography } from '@mui/material'
import Image from 'next/image'
import cls from './NewsDesc.module.scss'
import aboutImage from '../../public/images/banner1.png'
import { menu } from '../../mock/menu'
import { useRouter } from 'next/router'

function NewsDesc() {
  const { locale } = useRouter()
  return (
    <Box>
      <Typography variant='h1' color={`black`} className={cls.title}>
        {menu[locale].abouts}
      </Typography>
      <Box
        width='100%'
        mr={2}
        sx={{ background: '#ffffff' }}
        display='flex'
        flexDirection='column'
        className={cls.boxPadding}
      >
        <Image src={aboutImage} alt='Evos branch Image' />
        {/* <img
          src='/images/about.jpg'
          alt='Evos branch Image'
          className={cls.banner}
        /> */}
        <Box className={cls.boxMargin}>
          <Grid container spacing={2}>
            <Grid item xs={12} lg={12}>
              <Box mb={2}>
                <Typography variant='h24'>
                  EVOS – это самая большая сеть ресторанов быстрого обслуживания
                  в Узбекистане.
                </Typography>
              </Box>
              <Box mb={2}>
                <Typography variant='h24'>
                  Первый филиал компании был открыт в 2006 году и успешно
                  функционирует и по сей день! За 15 лет компания выросла из
                  небольшой закусочной на автобусной остановке в развернутую
                  сеть, включающую на сегодняшний день более 60 ресторанов по
                  всему Узбекистану, собственную службу доставки, современную
                  развернутую IT-инфраструктуру и более 2 000 сотрудников.
                </Typography>
              </Box>
              <Box mb={2}>
                <Box>
                  <Typography variant='h24'>Суть бренда EVOS:</Typography>
                </Box>
                <Typography variant='h24'>
                  Доступная современная еда и особая культура гостеприимства.
                </Typography>
              </Box>
              <Box mb={2}>
                <Box>
                  <Typography variant='h24'>Наша миссия:</Typography>
                </Box>
                <Typography variant='h24'>
                  Объединить самую популярную еду и лучшие традиции
                  гостеприимства, для создания незабываемого впечатления у наших
                  гостей.
                </Typography>
              </Box>
              <Box mb={2}>
                <Box>
                  <Typography variant='h24'>Наши цели:</Typography>
                </Box>
                <ul>
                  <li>
                    <Typography variant='h24'>
                      Стать лидером по изготовлению шаурмы на мировом рынке
                      ресторанов быстрого обслуживания
                    </Typography>
                  </li>
                  <li>
                    <Typography variant='h24'>
                      Использовать лучшие инновационные практики в сфере QSR,
                      сохраняя при этом ценовую доступность продукта
                    </Typography>
                  </li>
                  <li>
                    <Typography variant='h24'>
                      Открыть 50 000 ресторанов под брендом EVOS во всем мире
                    </Typography>
                  </li>
                </ul>
              </Box>
              <Box mb={2}>
                <Typography variant='h24'>
                  Мы всегда и во всем стараемся придерживаться главного
                  принципа, что все мы – это одна большая семья EVOS Oilasi, а
                  каждый сотрудник является важной и неотъемлемой частью общего
                  успеха компании.
                </Typography>
              </Box>
              <Box mb={2}>
                <Typography variant='h24'>
                  В построении любых отношений, будь то гости наших ресторанов,
                  деловые партнеры или сотрудники компании, мы стремимся
                  поддерживать принципы взаимоуважения, открытости, честности и
                  надежности. Благодаря этому за 15 лет, которые компания
                  существует на рынке, мы смогли завоевать и закрепить за собой
                  статус любимого заведения среди большого числа наших земляков,
                  надежного и долгосрочного партнера в деловых кругах, и
                  ответственного и стабильного работодателя в глазах наших
                  сотрудников и потенциальных соискателей.
                </Typography>
              </Box>
              <Box mb={2}>
                <Typography variant='h24'>
                  Мы по праву гордимся, что на сегодняшний день наша компания
                  является одним из крупнейших работодателей для студентов и
                  молодежи Узбекистана, благодаря тем условиям, которые мы
                  создаем для наших сотрудников.
                </Typography>
              </Box>
              <Box>
                <Typography variant='h24'>
                  Мы также с особой гордостью и вниманием относимся к
                  осуществлению проектов, имеющих социальную значимость. Мы
                  искренне верим, что сделать мир чуточку добрее по силам
                  каждому из нас, стоит только начать с себя. А когда мы
                  объединяем усилия и направляем их в общее доброе дело –
                  результаты превосходят даже самые смелые ожидания!
                </Typography>
              </Box>
            </Grid>
            {/* <Grid item xs={12} lg={6}>
              <img src='/images/about.jpg' alt='branch' className={cls.image} />
            </Grid> */}
          </Grid>
          {/*  */}
        </Box>
      </Box>
    </Box>
  )
}

export default NewsDesc
