import React, { useState } from 'react'
import cls from './MobileMenu.module.scss'
import AnchorLink from 'react-anchor-link-smooth-scroll'

function MobileMenu({ menu }) {
  const [activeLink, setActiveLink] = useState('')

  return (
    <div className={cls.root}>
      <div className={cls.menuListTop}>
        <ul className={cls.list}>
          {menu.map((item) => (
            <li key={item.id} className={cls.listItem}>
              <AnchorLink
                offset={110}
                href={`#${item.id}`}
                style={{ whiteSpace: 'nowrap' }}
                onClick={() => {
                  if (activeLink.includes(item.id)) {
                    setActiveLink('')
                  } else {
                    setActiveLink(String(item.id))
                  }
                }}
              >
                {item.title}
              </AnchorLink>
            </li>
          ))}
        </ul>
      </div>
    </div>
  )
}

export default MobileMenu
