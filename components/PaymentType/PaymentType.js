import React from 'react'
import { Typography, Box } from '@mui/material'
import CardWrapper from '../CardWrapper/CardWrapper'
import { CashIcon } from '../Icons'
import cls from './PaymentType.module.scss'
import { menu } from '../../mock/menu'
import { useRouter } from 'next/router'

export default function PaymentType({ formik }) {
  const { locale } = useRouter()
  return (
    <CardWrapper mt={2}>
      <Box display='flex' justifyContent='space-between'>
        <Typography variant={`h1`} color={`black`}>
          {menu[locale].typeOfPayment}
        </Typography>
      </Box>
      <Box className={cls.inputWrapper}>
        {/* <div className={cls.radioCard}>
          <input
            type='radio'
            name='paymentType'
            value='cash'
            id='cash'
            onChange={formik.handleChange}
          />
          <label htmlFor='cash'>
            <span className={cls.cardIcon}>
              <CashIcon />
            </span>
            <span className={cls.cardTitle}>Наличный</span>
            <span className={cls.radio} />
          </label>
        </div> */}

        <div className={cls.radioCard}>
          <input
            type='radio'
            name='paymentType'
            value='payme'
            id='payme'
            onChange={formik.handleChange}
            defaultChecked
          />
          <label htmlFor='payme'>
            <span className={cls.cardIcon}>
              <img src='/images/payme.png' alt='Payme' />
            </span>
            <span className={cls.cardTitle}>Payme</span>
            <span className={cls.radio} />
          </label>
        </div>

        <div className={cls.radioCard}>
          <input
            type='radio'
            name='paymentType'
            value='click'
            id='click'
            onChange={formik.handleChange}
          />
          <label htmlFor='click'>
            <span className={cls.cardIcon}>
              <img src='/images/click.png' alt='Click' />
            </span>
            <span className={cls.cardTitle}>Click</span>
            <span className={cls.radio} />
          </label>
        </div>
      </Box>
    </CardWrapper>
  )
}
