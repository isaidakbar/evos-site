import React, { useState } from 'react'
import cls from './Header.module.scss'
import { Container, Typography, Box } from '@mui/material'
import Region from '../Regions/Region'
import Link from 'next/link'
import { useRouter } from 'next/router'
import Image from 'next/image'
import { shallowEqual, useDispatch, useSelector } from 'react-redux'
import profile from '../../public/images/svg/profile.svg'
import LoginPopover from '../LoginPopover/LoginPopover'
import RightMenu from '../RightMenu/RightMenu'
import Navbar from '../Navbar/Navbar'
import basket from '../../public/images/svg/basket.svg'
import { EvosLogo, ExpandIcon, MenuIcon } from '../Icons'
import { cartTotalPriceSelector } from '../../store/selectors/cartSelectors'
import { numberToPrice } from '../../utils/numberToPrice'
import { openCartDrawer } from '../../store/actions/drawerActions/drawerActions'
import { menu } from '../../mock/menu'
import { useIsMobile } from '../../utils/useIsMobile'

function Header() {
  const [anchorEl, setAnchorEl] = useState(null)
  const [openRightMenu, setOpenRightMenu] = useState(false)
  const dispatch = useDispatch()
  const totalPrice = useSelector(
    (state) => cartTotalPriceSelector(state),
    shallowEqual
  )
  const { locale } = useRouter()
  const [isMobile] = useIsMobile(576)

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget)
  }

  const handleOpenDrawer = () => {
    dispatch(openCartDrawer())
  }

  const handleOpenRightMenu = () => {
    setOpenRightMenu(!openRightMenu)
  }

  const handleCloseRightMenu = () => {
    setOpenRightMenu(false)
  }

  const open = Boolean(anchorEl)
  const id = open ? 'simple-popover' : undefined

  return (
    <div className={cls.root} id='header'>
      <Container maxWidth='xl'>
        <Box display='flex' flexDirection='column'>
          <div className={cls.header}>
            <div className={cls.headerLeftBox}>
              <Link href={'/'}>
                <a style={{ display: 'flex' }}>
                  <EvosLogo className={cls.brandLogo} />
                </a>
              </Link>

              <Box ml={2}>
                <Region />
              </Box>
              <Box className={cls.navbar}>
                <Navbar />
              </Box>
            </div>
            <div className={cls.responsiveIconsWrapper}>
              <Box display='flex' alignItems='center' justifyContent='center'>
                {isMobile && (
                  <Box className={cls.basketMobile} id='basketMob'>
                    <button
                      className={`${cls.shop} ${totalPrice ? cls.total : ''}`}
                      onClick={handleOpenDrawer}
                      id='cartButton'
                    >
                      <div className={cls.shopping}>
                        <div className={cls.shopIcon}>
                          {totalPrice ? (
                            <img
                              src='/images/svg/basketWhite.svg'
                              width={24}
                              height={24}
                              alt='basket'
                            />
                          ) : (
                            <img
                              src='/images/svg/basket.svg'
                              width={24}
                              height={24}
                              alt='basket'
                            />
                          )}
                        </div>
                      </div>
                    </button>
                  </Box>
                )}
                <div
                  className={`${cls.menuIconBox} ${
                    openRightMenu && cls.rightMenu
                  }`}
                  onClick={handleOpenRightMenu}
                  onClose={handleCloseRightMenu}
                >
                  <MenuIcon />
                </div>
              </Box>
            </div>
            <div className={cls.headerRightBox}>
              <Box className={cls.basketDesktop} id='basketDesk'>
                <button
                  className={`${cls.shop} ${totalPrice ? cls.total : ''}`}
                  onClick={handleOpenDrawer}
                >
                  <div className={cls.shopping}>
                    {totalPrice ? (
                      <Box
                        display='flex'
                        alignItems='center'
                        justifyContent='space-between'
                        flexDirection='row-reverse'
                      >
                        <p className={cls.shopText}>
                          {numberToPrice(totalPrice)}
                        </p>
                        <img
                          src='/images/svg/basketWhite.svg'
                          width={24}
                          height={24}
                          alt='basket'
                        />
                      </Box>
                    ) : (
                      <Box
                        display='flex'
                        alignItems='center'
                        justifyContent='space-between'
                        flexDirection='row-reverse'
                      >
                        <p className={cls.shopText}>{menu[locale].basket}</p>
                        <Image
                          src={basket}
                          width={24}
                          height={24}
                          alt='basket'
                        />
                      </Box>
                    )}
                  </div>
                </button>
              </Box>

              {/* LANGUAGE SELECTION */}
              <div
                className={cls.lang}
                aria-describedby={id}
                onClick={handleClick}
              >
                {locale === 'uz' ? (
                  <img src='/images/svg/uz.svg' alt='uzbek' />
                ) : locale === 'en' ? (
                  <img src='/images/svg/en.svg' alt='english' />
                ) : (
                  <img src='/images/svg/ru.svg' alt='russian' />
                )}
                <div className={cls.expandIcon}>
                  <ExpandIcon />
                </div>
                <div className={cls.wrapper}>
                  <div className={cls.langList}>
                    <Link href='/' locale='uz'>
                      <a className={cls.listItem}>
                        <img src='/images/svg/uz.svg' alt='uzbekistan' />
                        <Typography
                          variant={`h4`}
                          sx={{ marginLeft: 2, textAlign: 'left' }}
                        >
                          O’zbekcha
                        </Typography>
                      </a>
                    </Link>
                    <Link href='/' locale='ru'>
                      <a className={cls.listItem}>
                        <img src='/images/svg/ru.svg' alt='russian' />
                        <Typography
                          variant={`h4`}
                          sx={{ marginLeft: 2, textAlign: 'left' }}
                        >
                          Русский
                        </Typography>
                      </a>
                    </Link>
                    <Link href='/' locale='en'>
                      <a className={cls.listItem}>
                        <img src='/images/svg/en.svg' alt='english' />
                        <Typography
                          variant={`h4`}
                          sx={{ marginLeft: 2, textAlign: 'left' }}
                        >
                          English
                        </Typography>
                      </a>
                    </Link>
                  </div>
                </div>
              </div>
              <div className={cls.user}>
                <Image src={profile} width={24} height={24} alt='profile' />
                <div className={cls.loginWrapper}>
                  <LoginPopover />
                </div>
              </div>
            </div>
          </div>
        </Box>
      </Container>

      <RightMenu open={openRightMenu} onClose={handleCloseRightMenu} />
    </div>
  )
}

export default Header
