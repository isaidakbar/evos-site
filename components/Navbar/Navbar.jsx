import React, { useState } from 'react'
import cls from './Navbar.module.scss'
import { Box } from '@mui/material'
import Link from 'next/link'
import { useRouter } from 'next/router'
import { menu } from '../../mock/menu'
import Login from '../Login/Login'

export default function Navbar() {
  const { pathname, locale } = useRouter()
  const [openLogin, setOpenDialog] = useState(false)

  const handleCloseLogin = () => {
    setOpenDialog(false)
  }

  return (
    <>
      <Box className={cls.root}>
        <div className={cls.footerBox}>
          <ul className={cls.footerList}>
            <li className={`${cls.listItem} ${cls.listLink}`}>
              <Link href='/'>
                <a className={`${pathname === '/' ? cls.activeLink : ''}`}>
                  {menu[locale].main}
                </a>
              </Link>
            </li>
            <li className={`${cls.listItem} ${cls.listLink}`}>
              <Link href='/branches' className={cls.listLink}>
                <a
                  className={`${
                    pathname.includes('/branches') ? cls.activeLink : ''
                  }`}
                >
                  {menu[locale].branches}
                </a>
              </Link>
            </li>
            <li className={`${cls.listItem} ${cls.listLink}`}>
              <Link href='/vacancy'>
                <a
                  className={`${
                    pathname.includes('/vacancy') ? cls.activeLink : ''
                  }`}
                >
                  {menu[locale].vacancies}
                </a>
              </Link>
            </li>
            <li className={`${cls.listItem} ${cls.listLink}`}>
              <Link href='/news'>
                <a
                  className={`${
                    pathname.includes('/news') ? cls.activeLink : ''
                  }`}
                >
                  {menu[locale].news}
                </a>
              </Link>
            </li>
            <li className={`${cls.listItem} ${cls.listLink}`}>
              <Link href='/about'>
                <a className={`${pathname === '/about' ? cls.activeLink : ''}`}>
                  {menu[locale].abouts}
                </a>
              </Link>
            </li>
            <li className={`${cls.listItem} ${cls.listLink}`}>
              <Link href='/contacts'>
                <a
                  className={`${
                    pathname === '/contacts' ? cls.activeLink : ''
                  }`}
                >
                  {menu[locale].contacts}
                </a>
              </Link>
            </li>
          </ul>
        </div>
      </Box>
      <Login
        open={openLogin}
        onClose={handleCloseLogin}
        setOpenLoginDialog={setOpenDialog}
      />
    </>
  )
}
