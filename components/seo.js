import Head from 'next/head'

export default function SEO({ title, description, image, keywords }) {
  return (
    <Head>
      <meta name='viewport' content='width=device-width, initial-scale=1' />
      <meta charSet='utf-8' />
      <title>{title ? `${title} | Evos` : 'Evos'}</title>
      <meta
        name='description'
        content={
          description ||
          'EVOS является прекрасным местом для встреч с друзьями, вкусной еды и отличного настроения. Меню состоит в основном из клаб сендвичей, хот-догов, бургеров, лавашей и донаров. Наши приоритеты – свежесть и качество ингредиентов, разнообразие начинок, доступные цены и внимание к просьбам гостей.'
        }
      />
      <meta
        name='keywords'
        content='MaxWay, доставка, еды, заказать,лаваш,клаб сендвич,лаваш,max way,way,max,maxway,maxwayuz,MaxWayUz'
      />
      <meta property='og:type' content='website' />
      <meta property='og:title' content='MaxWay' key='ogtitle' />
      <meta
        property='og:description'
        content={
          description ||
          'EVOS является прекрасным местом для встреч с друзьями, вкусной еды и отличного настроения. Меню состоит в основном из клаб сендвичей, хот-догов, бургеров, лавашей и донаров. Наши приоритеты – свежесть и качество ингредиентов, разнообразие начинок, доступные цены и внимание к просьбам гостей.'
        }
        key='ogdesc'
      />
      <meta property='og:site_name' content='EVOS' key='ogsitename' />
      {/* <meta
        property='og:image'
        content={image || 'https://maxway.uz/logo.svg'}
        key='ogimage'
      /> */}
      <meta name='twitter:card' content='summary' />
      <meta name='twitter:title' content={title ? `${title} | EVOS` : `EVOS`} />
      <meta
        name='twitter:description'
        content={
          description ||
          'EVOS является прекрасным местом для встреч с друзьями, вкусной еды и отличного настроения. Меню состоит в основном из клаб сендвичей, хот-догов, бургеров, лавашей и донаров. Наши приоритеты – свежесть и качество ингредиентов, разнообразие начинок, доступные цены и внимание к просьбам гостей.'
        }
      />
      <meta name='twitter:site' content='EVOS' />
      <meta name='twitter:creator' content='Udevs' />
      {/* <meta
        name='twitter:image'
        content={image || 'https://maxway.uz/logo.svg'}
      /> */}
      {/* <link rel='icon' href='/favicon.png' style={{ width: '100px' }} /> */}
    </Head>
  )
}
