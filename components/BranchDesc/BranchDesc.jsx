import React from 'react'
import { Box, Typography } from '@mui/material'
import CardWrapper from '../CardWrapper/CardWrapper'
import cls from './BranchDesc.module.scss'
import { YMaps, Map, ZoomControl, GeoObject } from 'react-yandex-maps'

function BranchDesc({ branchItem }) {
  const getLocation = branchItem.geoLocation.split(',')

  return (
    <div>
      <Typography
        variant={`h1`}
        color={`black`}
        className={cls.title}
        id='branchDescription'
      >
        {branchItem.title}
      </Typography>
      <Box display='flex' className={cls.verticalMargin}>
        <Box className={cls.maxWidthBox}>
          <CardWrapper>
            <Box display='flex' flexDirection='column' mt={3} mb={2}>
              <Box mb={3} mt={-3} className={cls.mobileTitle}>
                <Typography variant='h1'>{branchItem.title}</Typography>
              </Box>
              <div className={cls.branchBox}>
                <Box
                  display='flex'
                  flexWrap='nowrap'
                  justifyContent='space-between'
                  alignItems='center'
                  marginBottom='16px'
                >
                  <Typography variant={`h11`} sx={{ marginRight: '32px' }}>
                    Адрес
                  </Typography>
                  <Typography
                    variant={`h13`}
                    sx={{ width: '50%', textAlign: 'right' }}
                  >
                    {branchItem.address}
                  </Typography>
                </Box>
                <Box
                  display='flex'
                  flexWrap='nowrap'
                  justifyContent='space-between'
                  alignItems='center'
                  marginBottom='16px'
                >
                  <Typography variant={`h11`} sx={{ marginRight: '32px' }}>
                    Часы работы
                  </Typography>
                  <Typography
                    variant={`h3`}
                    sx={{ width: '50%', textAlign: 'right' }}
                  >
                    9:00 – 23:00
                  </Typography>
                </Box>
              </div>
              <Box marginBottom='20px'>
                <YMaps>
                  <Map
                    width='100%'
                    style={{ height: '300px' }}
                    defaultState={{
                      center: [
                        parseFloat(getLocation[0]),
                        parseFloat(getLocation[1]),
                      ],
                      zoom: 15,
                    }}
                  >
                    <GeoObject
                      options={{ iconColor: '#f5363e' }}
                      geometry={{
                        type: 'Point',
                        coordinates: [
                          parseFloat(getLocation[0]),
                          parseFloat(getLocation[1]),
                        ],
                      }}
                      properties={{ hintContent: branchItem.title }}
                    />
                    <ZoomControl
                      options={{
                        size: 'auto',
                        zoomDuration: 500,
                      }}
                    />
                  </Map>
                </YMaps>
              </Box>
            </Box>
          </CardWrapper>
        </Box>
      </Box>
    </div>
  )
}

export default BranchDesc
