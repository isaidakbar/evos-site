import React, { useState } from 'react'
import { Typography, Box } from '@mui/material'
import Image from 'next/image'
import { Logout } from '../Icons'
import { styled } from '@mui/material/styles'
import { useRouter } from 'next/router'
import order from '../../public/images/svg/order.svg'
import Login from '../Login/Login'
import {menu} from '../../mock/menu'
import { destroyCookie, parseCookies } from 'nookies'
import LoginForm from '../LoginForm/LoginForm'


const CustomBox = styled(Box)(({ theme }) => ({
  '& .MuiBox-root': {
    cursor: 'pointer',
    paddingTop: '6px',
    paddingBottom: '6px',
    paddingLeft: '8px',
    // 'paddingRight': '18px',
    '&:hover': {
      background: '#F6F8F9',
    },
  },
}))

function LoginPopover({ onClose, open }) {

  const router = useRouter()
  const {locale} = router
  const cookies = parseCookies()
  const [openMyInfo, setOpenMyInfo] = useState(false)
  const [openMyAddress, setOPenMyAddress] = useState(false)
  const [openLoginDialog, setOpenLoginDialog] = useState(false)

  if (openMyInfo === true) open = false
  if (openMyAddress === true) open = false

  // MY INFO DIALOG
  const handleOpenMyInfo = () => {
    setOpenMyInfo(true)
  }
  const handleCloseMyInfo = (e) => {
    e.preventDefault()
    setOpenMyInfo(false)
  }

  // MY ADDRESS DIALOG
  const handleOpenMyAddress = () => {
    setOPenMyAddress(true)
  }
  const handleCloseMyAddress = (e) => {
    e.preventDefault()
    setOPenMyAddress(false)
  }

  const handleRedirectToOrder = () => {
    if (cookies.client_id) {
      router.push('/orders')
      setOpenLoginDialog(false)
  } else setOpenLoginDialog(true)
  }


    const handleLogout = () => {
      destroyCookie(null, 'client_id', { path: '/', })
      localStorage.clear()
      window.location.href = '/'
    }

  // LOGIN DIALOG

  const handleCloseLogin = () => {
    setOpenLoginDialog(false)
  }

  return (
    <div>
      {/* <Popover
            //   id={id}
              open={open}
            //   anchorEl={anchorEl}
              onClose={onClose}
              anchorOrigin={{
                vertical: 'top',
                horizontal: 'center',
              }}
              sx={{ p: 2,
                position: {
                  left: '380px',
                  top: '60px'
                },
              '& .css-3bmhjh-MuiPaper-root-MuiPopover-paper': {
                boxShadow: '0' 
              },
            }}
            >
                <Box display='flex' flexDirection='column' width='232px' padding='16px'>
                    <CustomBox>
                        <Box display='flex' alignItems='center' onClick={handleRedirectToNews}>
                            <Box alignSelf='center' verticalAlign='middle'><Notification/></Box>
                            <Typography variant={`h4`} sx={{maxWidth: '144px', marginLeft: '12px'}}>Уведомления</Typography>
                        </Box>
                    </CustomBox>
                    <CustomBox>
                        <Box display='flex' alignItems='center' onClick={handleOpenMyInfo}>
                            <Box alignSelf='center' verticalAlign='middle'><Person/></Box>
                            <Typography variant={`h4`} sx={{maxWidth: '144px', marginLeft: '12px'}}>Мои данные</Typography>
                        </Box>
                    </CustomBox>
                    <CustomBox>
                        <Box display='flex' alignItems='center' onClick={handleOpenMyAddress}>
                            <Box alignSelf='center' verticalAlign='middle'><Address/></Box>
                            <Typography variant={`h4`} sx={{maxWidth: '144px', marginLeft: '12px'}}>Мои адреса</Typography>
                        </Box>
                    </CustomBox>
                    <CustomBox>
                        <Box display='flex' alignItems='center' onClick={handleRedirectToOrder}>
                            <Box alignSelf='center' verticalAlign='middle'><FastFood/></Box>
                            <Typography variant={`h4`} sx={{maxWidth: '144px', marginLeft: '12px'}} >Мои заказы</Typography>
                        </Box>
                    </CustomBox>
                    <CustomBox>
                        <Box display='flex' alignItems='center'>
                            <Box alignSelf='center' verticalAlign='middle'><Logout/></Box>
                            <Typography variant={`h4`} sx={{maxWidth: '144px', marginLeft: '12px'}}>Выйти</Typography>
                        </Box>
                    </CustomBox>
                    
                </Box>
              
            </Popover> */}

      <Box
        display='flex'
        flexDirection='column'
        width='232px'
        padding='16px'
        sx={{
          background: '#ffffff',
          boxShadow: '0px 8px 16px rgba(0, 0, 0, 0.06)',
        }}
      >
        {/* <CustomBox>
          <Box display='flex' alignItems='center' onClick={handleOpenMyInfo}>
            <Box alignSelf='center' verticalAlign='middle'>
              <Image src={profile} width={24} height={24} />
            </Box>
            <Typography
              variant={`h4`}
              sx={{ maxWidth: '144px', marginLeft: '12px' }}
            >
              Мои данные
            </Typography>
          </Box>
        </CustomBox>
        <CustomBox>
          <Box display='flex' alignItems='center' onClick={handleOpenMyAddress}>
            <Box alignSelf='center' verticalAlign='middle'>
            <Image src={address} width={24} height={24} />
            </Box>
            <Typography
              variant={`h4`}
              sx={{ maxWidth: '144px', marginLeft: '12px' }}
            >
              Мои адреса
            </Typography>
          </Box>
        </CustomBox> */}
        <CustomBox>
          <Box
            display='flex'
            alignItems='center'
            onClick={handleRedirectToOrder}
          >
            <Box alignSelf='center' verticalAlign='middle'>
            <Image src={order} width={24} height={24} />
            </Box>
            <Typography
              variant={`h4`}
              sx={{ maxWidth: '144px', marginLeft: '12px' }}
            >
              {menu[locale].myOrders}
            </Typography>
          </Box>
        </CustomBox>
        <CustomBox>
          <Box display='flex' alignItems='center' onClick={() => handleLogout()}>
          <Box alignSelf='center' verticalAlign='middle'>
              <Logout />
            </Box>
            <Typography
              variant={`h4`}
              sx={{ maxWidth: '144px', marginLeft: '12px' }}
            >
              {menu[locale].logout}
            </Typography>
          </Box>
        </CustomBox>
      </Box>
      {/* <MyInfoDialog onClose={handleCloseMyInfo} open={openMyInfo} />
      <MyAddress onClose={handleCloseMyAddress} open={openMyAddress} /> */}
      {/* <Login open={openLoginDialog} onClose={handleCloseLogin} setOpenLoginDialog={setOpenLoginDialog}/> */}
      <Login open={openLoginDialog} onClose={handleCloseLogin} setOpenLoginDialog={setOpenLoginDialog}/>
    </div>
  )
}

export default LoginPopover
