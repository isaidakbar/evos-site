import React from 'react'
import { Box, Typography } from '@mui/material'
import CardWrapper from '../CardWrapper/CardWrapper'
import Input from '../Input/Input'
// import SuccessStatus from '../SuccessStatus/SuccessStatus'
// import CanceledStatus from '../CanceledStatus/CanceledStatus'
import { useRouter } from 'next/router'
import ArrowIconEffect from '../ArrowIcon/ArrowIcon'
import cls from './OrderMain.module.scss'
import AcceptedStatus from '../Statuses/AcceptedStatus'
import CanceledStatus from '../Statuses/CanceledStatus'
import OnTheWayStatus from '../Statuses/OnTheWayStatus'
import ReadyStatus from '../Statuses/ReadyStatus'
import SuccessStatus from '../Statuses/SuccessStatus'
// import { RedCloseIcon } from '../Icons'
import { KnifeForkIcon, RedCloseIcon } from '../Icons'
import {
  Accepted,
  Rejected,
  OnTheWay,
  Ready,
  Delivered,
} from '../Statuses/StatusIcons'
import Link from 'next/link'
import moment from 'moment'
import 'moment/locale/ru'
import { menu, btnText } from '../../mock/menu'

function OrderMain({ list }) {
  const router = useRouter()
  const { locale } = router
  const handleRedirect = () => {
    router.push(`/orders/${list.id}`)
  }

  const getStatusComponent = (item) => {
    if (
      item.lastStatusId === 0 ||
      item.lastStatusId === 1 ||
      item.lastStatusId === 11 ||
      item.lastStatusId === 12 ||
      item.lastStatusId === 13 ||
      item.lastStatusId === 15
    ) {
      return <AcceptedStatus statusName={item.lastStatusTitle} />
    } else if (
      item.lastStatusId === 2 ||
      item.lastStatusId === 7 ||
      item.lastStatusId === 14
    ) {
      return <CanceledStatus statusName={item.lastStatusTitle} />
    } else if (
      item.lastStatusId === 10 ||
      item.lastStatusId === 13 ||
      item.lastStatusId === 8
    ) {
      return <OnTheWayStatus statusName={item.lastStatusTitle} />
    } else if (item.lastStatusId === 5 || item.lastStatusId === 6) {
      return <SuccessStatus statusName={item.lastStatusTitle} />
    } else if (item.lastStatusId === 9 || item.lastStatusId === 4) {
      return <ReadyStatus statusName={item.lastStatusTitle} />
    }
  }

  const getStatusComponentMobile = (item) => {
    if (
      item.lastStatusId === 0 ||
      item.lastStatusId === 1 ||
      item.lastStatusId === 11 ||
      item.lastStatusId === 12 ||
      item.lastStatusId === 13 ||
      item.lastStatusId === 15
    ) {
      return <Accepted />
    } else if (
      item.lastStatusId === 2 ||
      item.lastStatusId === 7 ||
      item.lastStatusId === 14
    ) {
      return <Rejected />
    } else if (
      item.lastStatusId === 10 ||
      item.lastStatusId === 13 ||
      item.lastStatusId === 8
    ) {
      return <OnTheWay />
    } else if (item.lastStatusId === 5 || item.lastStatusId === 6) {
      return <Delivered />
    } else if (item.lastStatusId === 9 || item.lastStatusId === 4) {
      return <Ready />
    }
  }

  moment.locale('ru')
  return (
    <div>
      <Typography variant={`h1`} color={`black`} className={cls.title}>
        {menu[locale].myOrders}
      </Typography>
      {list.map((item) => (
        <Link href={`/orders/${item.id}`}>
          <Box key={item.id} display='flex' className={cls.verticalMargin}>
            <Box className={cls.maxWidthBox} style={{ cursor: 'pointer' }}>
              <CardWrapper>
                <Box display='flex' justifyContent='space-between'>
                  <Typography variant={`h1`} color={`black`}>
                    {menu[locale].order} №{item.id}
                  </Typography>
                  <div>
                    <ArrowIconEffect />
                  </div>
                </Box>
                <Box className={cls.statusBox}>
                  <Box display='flex' mt={4} alignItems='center'>
                    {getStatusComponent(item)}
                    <Typography sx={{ marginLeft: 4 }}>
                      {moment(item.createdDate).format('LLL')}
                    </Typography>
                  </Box>
                </Box>
                <Box className={cls.statusBoxResponsive}>
                  <Box display='flex' mt={2} alignItems='center'>
                    <Box className={cls.iconWrapper}>
                      {getStatusComponent(item)}
                    </Box>
                    <Typography sx={{ marginLeft: 2 }}>
                      {moment(item.createdDate).format('LLL')}
                    </Typography>
                  </Box>
                </Box>
              </CardWrapper>
            </Box>
          </Box>
        </Link>
      ))}
    </div>
  )
}

export default OrderMain
